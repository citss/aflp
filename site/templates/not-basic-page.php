<?php namespace ProcessWire; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page->title ?></title>
    <meta name="description" content="Created to distill the lessons learnt and build upon the strengths of the UNITAR Hiroshima Afghan Fellowship. It aspires to channel the bonds, knowledge and energy of this worldwide community towards innovative and sustainable initiatives in Afghanistan.">
    <meta name="author" content="The Afghan Fellowship Legacy Projects (AFLP)">
    <meta name="keywords" content="UNITAR, AFLP, Afghan Fellowship Legacy Projects">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,700|Lexend+Deca&display=swap"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/main.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.theme.default.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates; ?>img/favicon.ico">
    <meta name="theme-color" content="#26292d">
</head>
<body>
<?php include "nav.php" ?>
    <section class="j_width dark j_header">
        <h1 class="main_title" data-aos="fade-down"  data-aos-delay="200"><?php echo $page->title ?></h1>
    </section>

    <section class="j_width" data-aos="fade-up"  data-aos-delay="500">
        <?php echo $page->body ?> 
    </section>

    <?php if($page->title == 'Community News'):?>
        <?php 
            $cn = $pages->find("template=community-news, sort=created");
            $cu_counter = 0;
        ?>

        <section class="j_width" style="margin-top: -6rem">
            <h2 class="cnp_title" class="cnp_title" class="seminar_row" data-aos="fade-up" data-aos-delay="200" data-aos-offset="0">Book Previews</h2>
            <div class="seminar_row" data-aos="fade-up" data-aos-delay="700" data-aos-offset="0">
                <?php 
                    foreach($cn as $child){
                        if(!$child->is_community_updates){
                            echo "<div class='seminar_col'>";
                            echo "<img src='".$child->main_photo->url."'/>";
                            echo "<h2>".$child->title."</h2>";
                            echo "<p>".$child->summary."</p>";
                            echo "<a href='".$child->url."'>Read more</a>";
                            echo "</div>";
                        }else{
                            $cu_counter++;
                        }
                    }
                ?>
                <div class='seminar_col'></div>
            </div>

            <?php if($cu_counter > 0): ?>
                <h2 class="cnp_title" class="seminar_row" data-aos="fade-up" data-aos-delay="200" data-aos-offset="0">Community Updates</h2>
                <div class="seminar_row" data-aos="fade-up" data-aos-delay="700" data-aos-offset="0">
                    <?php 
                        foreach($cn as $child){
                            if($child->is_community_updates){
                                echo "<div class='seminar_col'>";
                                echo "<img src='".$child->main_photo->url."'/>";
                                echo "<h2>".$child->title."</h2>";
                                echo "<p>".$child->summary."</p>";
                                echo "<a href='".$child->url."'>Read more</a>";
                                echo "</div>";
                            }
                        }
                    ?>
                    <div class='seminar_col'></div>
                </div>
            <?php endif ?>
        </section>

        
    <?php else: ?>
        
        <section class="j_width">
            <div class="seminar_row" data-aos="fade-up" data-aos-delay="700" data-aos-offset="0">
            <?php 
                foreach($page->children() as $child){
                    echo "<div class='seminar_col'>";
                    echo "<img src='".$child->main_photo->url."'/>";
                    echo "<h2>".$child->title."</h2>";
                    echo "<p>".$child->summary."</p>";
                    echo "<a href='".$child->url."'>Read more</a>";
                    echo "</div>";
                }

                $ue = $pages->find("template=upcoming-events, sort=-date");
                foreach($ue as $child){
                    echo "<div class='seminar_col'>";
                    echo "<img src='".$child->main_photo->url."'/>";
                    echo "<h2>".$child->title."</h2>";
                    echo "<p>".$child->summary."</p>";
                    echo "<a href='".$child->url."'>Read more</a>";
                    echo "</div>";
                }
            ?>
            <div class='seminar_col'></div>
            </div>
        </section>

        <?php 
                $toggle = $pages->find("template=main_with_toggle");
                foreach($toggle as $child){
                    if($child->cb_toggle){
        ?>

        <!-- <section class="j_width">
            <h1 class="main_title"><?php echo $child->title ?></h1>
            <div class="seminar_row" >
            <?php 
                $ue = $pages->find("template=upcoming-events, sort=-date");
                foreach($ue as $child){
                    echo "<div class='seminar_col'>";
                    echo "<img src='".$child->main_photo->url."'/>";
                    echo "<h2>".$child->title."</h2>";
                    echo "<p>".$child->summary."</p>";
                    echo "<a href='".$child->url."'>Read more</a>";
                    echo "</div>";
                }
            ?>
            <div class='seminar_col'></div>
            </div>
        </section> -->

        <?php
                }
            }
        ?>
    <?php endif ?>

    <!-- <section class="j_width" >
        <div class="bot_nav">

            <a href="<?php $main = $page->parent()->parent()->children(); echo $main->get(4)->url;?>">Secretariat/Contacts&nbsp;<i class='fas fa-chevron-right'></i></a>

        </div>
    </section> -->


    <section class="j_width" >
        <div class="bot_nav">
            <?php

                if($page->prev->url){
                    echo "<a href='".$page->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->prev->title."</a>";
                }else{
                    echo "<a href='".$page->parent()->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->parent()->prev->title."</a>";
                }

                if($page->next->url){
                    if(count($page->next->children()) == 0){
                        echo "<a href='".$page->next->url."'>".$page->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }else{
                        echo "<a href='".$page->next->children()->first()->url."'>".$page->next->children()->first()->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }
                }else{
                    echo "<a href='".$page->parent()->next->url."'>".$page->parent()->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                }
            ?>
        </div>
    </section>
    

    <?php include "footer.php" ?>

 
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> 
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/owl.carousel.min.js"></script>
    
</body>
</html>