<link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/nav.css">
<?php 

$level = count($page->parents);

if($level === 0){
    $main = $page->children();
    $d1 = 'data-aos="fade-down"  data-aos-delay="1000"';
    $d2 = 'data-aos="fade-down"  data-aos-delay="1200"';
    $d3 = 'data-aos="fade-down"  data-aos-delay="1400"';
    $d4 = 'data-aos="fade-down"  data-aos-delay="1600"';
    $d5 = 'data-aos="fade-down"  data-aos-delay="1800"';
    $d6 = 'data-aos="fade-down"  data-aos-delay="2000"';
}elseif($level === 1){
    $main = $page->parent()->children();
    $d1 = 'data-aos="fade-down"  data-aos-delay="1000"';
    $d2 = 'data-aos="fade-down"  data-aos-delay="1200"';
    $d3 = 'data-aos="fade-down"  data-aos-delay="1400"';
    $d4 = 'data-aos="fade-down"  data-aos-delay="1600"';
    $d5 = 'data-aos="fade-down"  data-aos-delay="1800"';
    $d6 = 'data-aos="fade-down"  data-aos-delay="2000"';
}elseif($level === 2){
    $main = $page->parent()->parent()->children();
    $d1 = 'data-aos="fade-down"';
    $d2 = 'data-aos="fade-down"  data-aos-delay="100"';
    $d3 = 'data-aos="fade-down"  data-aos-delay="200"';
    $d4 = 'data-aos="fade-down"  data-aos-delay="400"';
    $d5 = 'data-aos="fade-down"  data-aos-delay="600"';
    $d6 = 'data-aos="fade-down"  data-aos-delay="800"';
}elseif($level === 3){
    $main = $page->parent()->parent()->parent()->children();
    $d1 = 'data-aos="fade-down"';
    $d2 = 'data-aos="fade-down"  data-aos-delay="100"';
    $d3 = 'data-aos="fade-down"  data-aos-delay="200"';
    $d4 = 'data-aos="fade-down"  data-aos-delay="400"';
    $d5 = 'data-aos="fade-down"  data-aos-delay="600"';
    $d6 = 'data-aos="fade-down"  data-aos-delay="800"';
}elseif($level === 4){
    $main = $page->parent()->parent()->parent()->parent()->children();
    $d1 = 'data-aos="fade-down"';
    $d2 = 'data-aos="fade-down"  data-aos-delay="100"';
    $d3 = 'data-aos="fade-down"  data-aos-delay="200"';
    $d4 = 'data-aos="fade-down"  data-aos-delay="400"';
    $d5 = 'data-aos="fade-down"  data-aos-delay="600"';
    $d6 = 'data-aos="fade-down"  data-aos-delay="800"';
}

$hasCarousel = ['UNITAR Hiroshima Afghan Fellowship (AF)', 'Partners', 'Cycles', 'Introduction', 'Description and Structure'];


// if(!$user->isLoggedin()) {
//     if(strpos($_SERVER['REQUEST_URI'],'webdev')){
//         $session->redirect("/webdev/aflp/login"); 
//     }else if($_SERVER['REQUEST_URI'] == '/aflp/'){
//         $session->redirect("/aflp/login"); 
//     }else{
//         $session->redirect("/login");
//     }
// }
?>


<div class="full_nav j_width">

    <div class="nav_left">
        <div class="logos">
            <a href="<?php echo $main->get(0)->url; ?>" class="aflp_logo" <?php echo $d1 ?>
            ><img src="<?php echo $config->urls->templates; ?>img/aflp_logo.svg" alt="AFLP Logo">
            </a>
            <a href="<?php echo $main->get(0)->url; ?>" class="unitar_logo" <?php echo $d1 ?>
            ><img src="<?php echo $config->urls->templates; ?>img/unitar_logo.svg" alt="UNITAR Logo">
            </a>
            
        </div>

    </div>

    <div class="nav_right">
        <div class="hamburger"  <?php echo $d1 ?>>
            <p>MENU</p>
            <div class="lines">
                <span></span>
                <span></span>
            </div>
        </div>


        <div class="nav_items">

            <a class="nav_item" href="<?php echo $main->get(0)->url;?>"><span><?php echo $main->get(0)->title;?></span></a>
            <!-- <div class="nav_item"><span class="home_menu"><i class="fas fa-chevron-right"></i>&nbsp;Home</span>
                <div class="nav_item_submenu">
                    <a href="<?php echo $main->get(0)->url; ?>">Home</a>
                    <?php 
                        $home = $pages->find("template=home");
                        foreach($home as $h){
                            foreach($h->multi_section as $sect){
                                echo '<a href="'.$main->get(0)->url.'#'.strtolower (str_replace(' ', '_', $sect->title)).'">'.$sect->title.'</a>';
                            }
                        }
                    ?>
                </div>
            </div> -->

            <div class="nav_item"><span><i class="fas fa-chevron-right">&nbsp;</i><?php echo $main->get(1)->title;?></span>

                <div class="nav_item_submenu">

                    <?php 
                        foreach($main->get(1)->children() as $p){
                            if($p->title === "Partners"){
                                echo '<div class="sub_nav_item"><i class="fas fa-chevron-right"></i>&nbsp;'.$p->title.'</div>';
                                echo '<div class="nav_item_sub_submenu">';
                                foreach($p->children() as $smenu){
                                    if($smenu->hasChildren()){
                                        echo '<div class="sub_nav_item"><i class="fas fa-chevron-right"></i>&nbsp;'.$smenu->title.'</div>';
                                        echo '<div class="nav_item_sub_submenu">';
                                        foreach($smenu->children() as $ssmenu){
                                            echo '<a href="'.$ssmenu->url.'">'.$ssmenu->title.'</a>';
                                        }
                                        echo '</div>';
                                    }else if(strpos($smenu->title, 'University') !== false){ 
                                        echo '<a href="'.$smenu->url.'">&nbsp;&nbsp;-&nbsp;'.$smenu->title.'</a>';

                                    }else{
                                        echo '<a href="'.$smenu->url.'">'.$smenu->title.'</a>';

                                    }
                                }
                                echo '</div>';
                            }else{
                                echo '<a href="'.$p->url.'">'.$p->title.'</a>';

                            }
                        }
                    ?>
                    <!-- <a href="https://www.edenseminars.org/new-page-1" target="_blank">Resources</a> -->
                </div>
            </div>

            <div class="nav_item"><span class="book_menu"><i class="fas fa-chevron-right"></i>&nbsp;<?php echo $main->get(2)->title;?></span>
                <div class="nav_item_submenu">
                    <a href="<?php echo $main->get(2)->url;?>#book">Book</a>
                    <a href="<?php echo $main->get(2)->url;?>#introduction">Introduction</a>
                    <a href="<?php echo $main->get(2)->url;?>#call_for_papers">Call for Papers</a>
                    <?php foreach($main->get(2)->children() as $b):?>
                        <a href="<?php echo $b->url; ?>"><?php echo $b->title; ?></a>
                    <?php endforeach?>

                    <!-- <a href="<?php echo $main->get(2)->url;?>#blog">Blog</a>
                    <a href="<?php echo $main->get(2)->url;?>#publication_and_media">Publication and Media</a> -->
                </div>
            </div>

            <a class="nav_item" href="<?php echo $main->get(3)->url;?>"><span><?php echo $main->get(3)->title;?></span></a>

            <div class="nav_item"><span><i class="fas fa-chevron-right">&nbsp;</i><?php echo $main->get(4)->title;?></span>
                <div class="nav_item_submenu">
                    <?php 
                        foreach($main->get(4)->children() as $p){
                            echo '<a href="'.$p->url.'">'.$p->title.'</a>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Partners Side Section -->
<?php 
    //Display only in BGnet page
    // if (strpos($_SERVER['REQUEST_URI'], "legacy-project-bg-net") !== false){
    //     echo '<div class="partner_side_section">';
    //     echo '<div class="toggle_btn"><i class="fas fa-chevron-right"></i></div>';

    //     $partners = $pages->find("template=partners-logo");
    //     foreach($partners as $p){
    //         foreach($p->partners_logo as $pl){
    //             echo '<div class="partner_logo" onclick="goToPage(\''.$pl->link.'\')">';
    //             echo "<img src='".$pl->logo->url."'/>";
    //             echo "<h2>".$pl->title."</h2>";
    //             echo "</div>";
    //         }
    //     }
    //     echo '</div>';
    // }
?>

<div class="intro_backdrop"></div>
