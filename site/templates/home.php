<?php namespace ProcessWire; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AFLP - Home</title>
    <meta name="description" content="Created to distill the lessons learnt and build upon the strengths of the UNITAR Hiroshima Afghan Fellowship. It aspires to channel the bonds, knowledge and energy of this worldwide community towards innovative and sustainable initiatives in Afghanistan.">
    <meta name="author" content="The Afghan Fellowship Legacy Projects (AFLP)">
    <meta name="keywords" content="UNITAR, AFLP, Afghan Fellowship Legacy Projects">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,700|Lexend+Deca&display=swap"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/main.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates; ?>img/favicon.ico">
    <meta name="theme-color" content="#26292d">
</head>
<body>
    <?php include "nav.php"; ?>
    <div class="hero j_width">
        <!-- <video autoplay muted loop id="video_bg">
            <source src="<?php echo $config->urls->templates; ?>img/bg.mp4" type="video/mp4">
        </video> -->

        <!-- <div id="hero_imgs">
            <img class="active" src="<?php echo $config->urls->templates; ?>img/aflp1.jpg" />
            <img src="<?php echo $config->urls->templates; ?>img/aflp2.jpg" />
            <img src="<?php echo $config->urls->templates; ?>img/aflp3.jpg" />
        </div> -->

        <div class="slider_wrapper">

            <div class="bg-carousel-wrapper bcwl">
                <div class="bg-carousel hcl">
                    <?php
                        $carousel = $pages->find("template=carousel-home-left");
                        foreach($carousel as $c){
                            foreach($c->carousel_item as $item){ 
                                echo '<div class="item">';
                                echo '<img src="'.$item->image->url.$item->image.'" alt="'.$item->place_name.'"/>';
                                echo '</div>'; 
                            }
                        }
                    ?>
                </div>
            </div>

            <div class="bg-carousel-wrapper">
                <div class="bg-carousel">
                    <?php
                        $carousel = $pages->find("template=carousel-home");
                        foreach($carousel as $c){
                            foreach($c->carousel_item as $item){ 
                                echo '<div class="item">';
                                echo '<img src="'.$item->image->url.$item->image.'" alt="'.$item->place_name.'"/>';
                                echo '</div>'; 
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        
        
        <div class="hero_content">
            
            <h1 data-aos="fade-up" data-aos-delay="1000">The Afghan Fellowship<br>Legacy Projects</h1>
            <!-- The Afghan Fellowship Legacy Projects (AFLP) was  -->
            <p data-aos="fade-up" data-aos-delay="1200">created to distill the lessons learnt and build upon the strengths of the UNITAR Hiroshima Afghan Fellowship. It aspires to channel the bonds, knowledge and energy of this worldwide community towards innovative and sustainable initiatives in Afghanistan.</p>

        </div>
    </div>

    <!-- <?php 
            $toggle = $pages->find("template=main_with_toggle");
            foreach($toggle as $child){
                if($child->cb_toggle){
    ?> -->

    <!-- <section class="j_width_home">
        <h1 class="main_title mt">Upcoming Events</h1>
        <div class="upcoming_event_row">
        <?php 
            $ue = $pages->find("template=upcoming-events, sort=created");
			foreach($ue as $child){
                echo "<div class='upcoming_event_col'>";
				echo "<div class='uec_img'>";
                echo "<img src='".$child->main_photo->url."'/>";
				echo "<div class='uec_date'>";
                echo "<span class='month'>".date("M",strtotime($child->date))."</span>";
                echo "<span class='day'>".date("d",strtotime($child->date))."</span>";
                echo "<span class='year'>".date("Y",strtotime($child->date))."</span>";
                echo "</div>";
                echo "</div>";
				echo "<div class='uec_text'>";
                echo "<h2>".$child->title."</h2>";
                echo "<a href='".$child->url."'>Learn More</a>";
                echo "</div>";
                echo "</div>";
			}
        ?>
        <div class='upcoming_event_col dummy' style="background-color: transparent; box-shadow: none"></div>
        </div>
    </section> -->

    <!-- <?php
            }
        }
    ?> -->

    

    <?php
        $j_width_home = '';
        $upevent = false;
        $upevent_title = '';
        $toggle = $pages->find("template=main_with_toggle");
        foreach($toggle as $child){
            if($child->cb_toggle){
                $upevent = true;
                $upevent_title = $child->title;
                $j_width_home = 'j_width_home';
            }
        }

        $cu_counter = 0;
        $not_cu_counter = 0;

        $home = $pages->find("template=home");
        $i = 0;
        $ii = 1;
        $iii = 1;
        echo '<section class="j_width '.$j_width_home.' section_home" id="'.strtolower (str_replace(' ', '_', $sect->title)).'">';

            echo '<div class="upevet">';
            echo '<div class="upevet_content">';
            // $ue = $pages->find("template=upcoming-events, sort=-date");
			// foreach($ue as $child){
            //     echo "<div class='upcoming_event_col'>";
			// 	echo "<div class='uec_img'>";
            //     echo "<img src='".$child->main_photo->url."'/>";
            //     echo "</div>";
			// 	echo "<div class='uec_text'>";
            //     echo '<div>';   
            //     echo "<h2>".$child->title." isi</h2>";
            //     echo "<h5>".date('j F Y',strtotime($child->date))."</h5>";
            //     echo '</div>';   
            //     echo "<a href='".$child->url."'>Learn More</a>";
            //     echo "</div>";
            //     echo "</div>";
			// }
            $cn = $pages->find("template=community-news, sort=-date");
            echo '<h1>BG-Net Activities</h1>';



            

            echo "<div class='owl-carousel owl-theme' id='carousel_comm_main_1'>";
                if(count($cn) > 0){
                    foreach($cn as $child){
                        if(!$child->is_community_updates){
                            echo "<div class='item up_item'>";
                            echo "<div class='upcoming_event_col'>";
                            echo "<div class='uec_img'>";
                            echo "<img src='".$child->main_photo->url."'/>";
                            echo "</div>";
                            echo "<div class='uec_text'>";
                            echo '<div>';   
                            echo "<h2>".$child->title."</h2>";
                            echo "<h5>".date('j F Y',strtotime($child->date))."</h5>";
                            echo '</div>';   
                            echo "<a href='".$child->url."'>Learn More</a>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";

                            if ($ii++ == 3) break;
                        }else{
                            $cu_counter++;
                        }
                    }
                }
            echo "</div><br><br>";


            if($cu_counter > 0){
                echo '<h1>Community News</h1>';
            

            echo "<div class='owl-carousel owl-theme' id='carousel_comm_main_2'>";
                if(count($cn) > 0){
                    foreach($cn as $child){
                        if($child->is_community_updates){
                            echo "<div class='item up_item'>";
                            echo "<div class='upcoming_event_col'>";
                            echo "<div class='uec_img'>";
                            echo "<img src='".$child->main_photo->url."'/>";
                            echo "</div>";
                            echo "<div class='uec_text'>";
                            echo '<div>';   
                            echo "<h2>".$child->title."</h2>";
                            echo "<h5>".date('j F Y',strtotime($child->date))."</h5>";
                            echo '</div>';   
                            echo "<a href='".$child->url."'>Learn More</a>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";

                            if ($iii++ == 3) break;
                        }
                    }
                }
            echo "</div>";

            } 
            
            echo '</div>';   
            echo '</div>';


        foreach($home as $h){
            foreach($h->multi_section as $sect){
                $i++;

                if($sect->checkbox === 0){
                    echo '<h1 class="main_title">'.$sect->title.'</h1>';
                }
                echo $sect->body;

                echo '<div class="owl-carousel owl-theme" id="carousel_main">';
                $carousel = $pages->find("template=carousel, name=carousel-middle");
                foreach($carousel as $c){
                    foreach($c->carousel_item as $item){ 
                        echo '<div class="item">';
                        echo '<img src="'.$item->image->url.$item->image.'" alt="'.$item->place_name.'"/>';
                        // echo '<h5><span>'.$item->place_name.'&nbsp;|&nbsp;</span>'.$item->location.'</h5>';
                        echo '<h5></h5>';
                        echo '</div>'; 
                    }
                }
                echo '</div>'; 
            
            }

        }
        echo '</section>';

       
    ?>

    <section class="j_width <?php echo $j_width_home ?>">
        <?php
            if($upevent){
                echo '<div class="footnavbar_w">';
                echo '<div class="footnavbar"></div>';
                echo '</div>';
            }
        ?>
        <div class="bot_nav">
            <?php


                $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                $uri_segments = explode('/', $uri_path);

                if(isset($uri_segments[2])){
                    if($page->prev->url){
                        echo "<a href='".$page->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->prev->title."</a>";
                    }else if($page->prev->title != ""){
                        echo "<a href='".$page->parent()->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->parent()->prev->title."</a>";
                    }
    
                    if($page->next->url){
                        if(count($page->next->children()) == 0){
                        echo "<a href='".$page->next->url."'>".$page->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                        }else{
                            echo "<a href='".$page->next->children()->first()->url."'>".$page->next->children()->first()->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                        }
                    }
                }else{
                    echo "<a href='".$page->child()->next->children()->first()->url."'>".$page->child()->next->children()->first()->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                }


                
            ?>
        </div>
    </section>

      
    <?php include "footer.php" ?>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> 
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/owl.carousel.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
    <script>
        $(document).ready(function(){
            $('.bg-carousel').slick({
                vertical: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                adaptiveHeight: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 3000,
                speed: 3000,
                arrows: false,
            })
        })
    </script>
</body>
</html>