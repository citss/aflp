<?php namespace ProcessWire; ?>
<?php 
    echo '<section class="carousel carousel_home">';
    echo '<div data-aos="fade-up"  data-aos-delay="1300">';
    echo '<div class="owlnav">';
    echo '<span class="nav_left_carousel"> <i class="fas fa-chevron-left"></i></span>';
    echo '<i class="fas fa-grip-lines-vertical"></i>';
    echo '<span class="nav_right_carousel"> <i class="fas fa-chevron-right"></i></span>';
    echo '</div>';
    echo '<div class="owl-carousel owl-theme">';
    $carousel = $pages->find("template=carousel-home");
    foreach($carousel as $c){
        foreach($c->carousel_item as $item){ 
            echo '<div class="item">';
            echo '<img src="'.$item->image->url.$item->image.'" alt="'.$item->place_name.'"/>';
            // echo '<h5><span>'.$item->place_name.'&nbsp;|&nbsp;</span>'.$item->location.'</h5>';
            echo '<h5></h5>';
            echo '</div>'; 
        }
    }
    echo '</div>'; 
    echo '</div>'; 
    echo '</section>';
    echo '<div class="rect"></div>';
?>