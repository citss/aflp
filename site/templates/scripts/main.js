var mobile = window.matchMedia("(max-width: 900px)")

$(document).ready(function(){

    setTimeout(() => {
        $(".intro_backdrop").fadeOut()
    }, 1000);

    AOS.init({
        duration: 450,
        easing: 'ease-in-out-quart'
    });
    setInterval('cycleImages()', 7000);

    $(".hero #video_bg").css("transform","scale(1)")
    $(".intro img").css("transform","translateY(-100%)")
    $(".intro").css("height","0")


    $('section p img, section img').attr("width","");

    $('#carousel_main').owlCarousel({
        items:3,
        loop:true,
        margin: 15,
        nav: false,
        dots: false,
        autoplay: 5000,
        autoplaySpeed: 3000,
        autoplayTimeout: 5000,
        autoplayHoverPause:true,
        lazyLoad: true,
        responsive : {
            0 : {
                items:1,
            },
            700 : {
                items:2,
            },
            900 : {
                items:2,
            },
            1350 : {
                items:3,
            }
        }
    });


    $("#carousel_comm_main_1, #carousel_comm_main_2").owlCarousel({
        items:1,
        loop:true,
        nav: true,
        dots: true,
        lazyLoad: true,
    });

    

    $('.nav_left_carousel').click(function() {
        $('#carousel_main').trigger('prev.owl.carousel');
    })

    $('.nav_right_carousel').click(function() {
        $('#carousel_main').trigger('next.owl.carousel');
    })


    $('#partner_carousel').owlCarousel({
        items: 3,
        loop: true,
        autoplay: true, 
        nav:false,
        dots:false,
        autoplay: true,
        slideTransition: 'linear',
        autoplayTimeout: 3500,
        smartSpeed: 3500,
        autoplayHoverPause: true,
        margin: 0,
        autoWidth:true,
        mouseDrag: false,
        touchDrag: false,
    });


    $(".scroll_to_top").click(function(){
        $("html, body").animate({ scrollTop: 0 }, 1000);
    })

    //Nav
    let hambToggled = false;
    $(".hamburger").click(function(){
        if(!hambToggled){
            $(this).find("span:first").css({
                "transform": "rotate(45deg) translateY(4px)",
            })
    
            $(this).find("span:last").css({
                "transform": "rotate(-45deg) translate(1.5px, -5px)"
            })
    
            $(this).find("p").text("CLOSE")

            $(".nav_items").css("maxHeight","1000px")


            $(this).css("background-color","rgb(32, 32, 32)")

            hambToggled = true;
        }else{
            $(".nav_items").css("maxHeight","0px")

            $(this).find("span:first").css({
                "transform": "rotate(0) translateY(0)",
            })
    
            $(this).find("span:last").css({
                "transform": "rotate(0) translate(0, 0)"
            })

            $(this).find("p").text("MENU")

            setTimeout(() => {
                $(this).css("background-color","transparent")
            }, 500);

            $(".nav_item .nav_item_submenu").removeClass("maxH")

            // $(".home_menu").html("<i class='fas fa-chevron-right'></i>&nbsp;Home")
            $(".book_menu").html("<i class='fas fa-chevron-right'></i>&nbsp;Book")
            hambToggled = false;
        }
    })

    $(".nav_item").click(function(e){
        // $(".home_menu").html("<i class='fas fa-chevron-right'>&nbsp;</i>&nbsp;Home")
        $(".book_menu").html("<i class='fas fa-chevron-right'>&nbsp;</i>&nbsp;Book")
        if($(this).find("span").text().includes("Home")){
            // $(this).find("span").html("<i class='fas fa-chevron-right'>&nbsp;</i>AFLP")
        }else if($(this).find("span").text().includes("Book")){
            $(this).find("span").html("<i class='fas fa-chevron-right'>&nbsp;</i>AFLP")
        }

        if($(this).find(".nav_item_submenu").hasClass("maxH")){
            if(!$(e.target).closest('.sub_nav_item').length && !$(e.target).is('.sub_nav_item')) {
                $(".nav_item .nav_item_submenu").removeClass("maxH")
                // $(".home_menu").html("<i class='fas fa-chevron-right'>&nbsp;</i>Home")
                $(".book_menu").html("<i class='fas fa-chevron-right'>&nbsp;</i>Book")
            }  
        }else{
        $(".nav_item_sub_submenu").removeClass("maxH")
        $(".nav_item .nav_item_submenu").removeClass("maxH")
            $(this).find(".nav_item_submenu").addClass("maxH")
        }

    })

    $(".sub_nav_item").click(function(){
        $(this).next(".nav_item_sub_submenu").toggleClass("maxH")
    })

    $("body").click(function(e){
        if(!$(e.target).closest('.hamburger, .nav_item').length && !$(e.target).is('.hamburger, .nav_item')) {
            $(".nav_items").css("maxHeight","0px")

            $(".hamburger").find("span:first").css({
                "transform": "rotate(0) translateY(0)",
            })
    
            $(".hamburger").find("span:last").css({
                "transform": "rotate(0) translate(0, 0)"
            })

            $(".hamburger").find("p").text("MENU")

            setTimeout(() => {
                $(".hamburger").css("background-color","transparent")
            }, 500);

            $(".nav_item .nav_item_submenu").removeClass("maxH")

            // $(".home_menu").html("<i class='fas fa-chevron-right'></i>&nbsp;Home")
            $(".book_menu").html("<i class='fas fa-chevron-right'></i>&nbsp;Book")
            hambToggled = false;    
        } 
    })

    $(".full_nav a").click(function(){
        $(".nav_items").css("maxHeight","0px")

        $(".hamburger").find("span:first").css({
            "transform": "rotate(0) translateY(0)",
        })

        $(".hamburger").find("span:last").css({
            "transform": "rotate(0) translate(0, 0)"
        })

        $(".hamburger").find("p").text("MENU")

        setTimeout(() => {
            $(".hamburger").css("background-color","transparent")
        }, 500);

        $(".nav_item .nav_item_submenu").removeClass("maxH")

        // $(".home_menu").html("<i class='fas fa-chevron-right'>&nbsp;</i>&nbsp;Home")
        $(".book_menu").html("<i class='fas fa-chevron-right'>&nbsp;</i>&nbsp;Book")
        hambToggled = false;    
    })

    //Bot nav
    var bot_Atags = $(".bot_nav a").length;
    if(bot_Atags === 1){
        if($(".bot_nav a i").attr("class").includes("right")){
            $(".bot_nav").css("justify-content","flex-end")
        }else{
            $(".bot_nav").css("justify-content","flex-start")
        }
    }

    //Toggle side nav partner
    // var thisToggled = false;
    // $(".toggle_btn").click(function(){
    //     if(!thisToggled){
    //         $(".partner_side_section").css("right","calc((-60px - 1rem))")
    //         $(".partner_side_section .toggle_btn i").attr("class","fas fa-chevron-left")
    //         thisToggled = true
    //     }else{
    //         $(".partner_side_section").css("right","0")
    //         $(".partner_side_section .toggle_btn i").attr("class","fas fa-chevron-right")
    //         thisToggled = false
    //     }
        
    // })
})

$(window).resize(function(){
    if(mobile.matches){
        $(".full_nav").css("background-color", "#108d30")
        $(".full_nav .unitar_logo, .full_nav .unitar_logo img").css("height","70px")
    }else{
        $(".full_nav").css("background-color", "tannsparent")
        $(".full_nav .unitar_logo, .full_nav .unitar_logo img").css("height","70px")
    }
})  


$(window).ready(function(){
    var st = $(this).scrollTop();

    if (st > (($("section").offset().top + $("section").outerHeight(true)) )) {
        $(".full_nav").css({
            "position": "fixed",
            "height": "100px",
            "background-color": "#108d30",
            "top": "-100px",
        })
    //    $(".full_nav .unitar_logo, .full_nav .unitar_logo img").css("height","70px")

    }

    if (st > ($("section").offset().top + $("section").outerHeight(true))) {
        //
    }else{
        $(".full_nav").css({
            "position": "absolute",
            "height": mobile.matches ? "100px" : "100px",
            "background-color": mobile.matches ? "#108d30":"transparent",
            "top": "0"
        })
    //    $(".full_nav .unitar_logo, .full_nav .unitar_logo img").css("height", mobile.matches ? "70px" : "70px")

    }
})

var lastScrollTop = 0;
$(window).scroll(function(){

    var st = $(this).scrollTop();

    
    if (st > (($("section").offset().top + $("section").outerHeight(true)) )) {
        $(".full_nav").css({
            "position": "fixed",
            "height": "100px",
            "background-color": "#108d30",
            "top": "-100px",
        })
    //    $(".full_nav .unitar_logo, .full_nav .unitar_logo img").css("height","70px")



    }

    if (st > ($("section").offset().top + $("section").outerHeight(true))) {


        if (st > lastScrollTop){
            $(".full_nav").css({
                "transition": "top 300ms ease-out",
            })
            $(".full_nav").css({
                "top": "0",
            })
            
        }else{
            $(".full_nav").css({
                "top": "-100px",
            })
        }
        lastScrollTop = st;
    }else{
        $(".full_nav").css({
            "position": "absolute",
            "height": mobile.matches ? "100px" : "100px",
            "background-color": mobile.matches ? "#108d30":"transparent",
            "top": "0"
        })
    //    $(".full_nav .unitar_logo, .full_nav .unitar_logo img").css("height", mobile.matches ? "70px" : "70px")

    }


    
});

function toggleNav(){
    if(!mainClicked){
        $(".hamb i").css("transform","rotate(90deg)")
        $(".hamb i").attr("class","fas fa-times")
        $(".full_nav_bg").css({"background":"#26292d","box-shadow":"0 0 5px rgba(0,0,0,.8)"})
        $(".full_nav .nav_items").css("height","auto")
        mainClicked = true;
    }else{
        $(".hamb i").css("transform","rotate(0deg)")
        $(".hamb i").attr("class","fas fa-bars")
        $(".full_nav_bg").css({"background":"transparent","box-shadow":"none"})
        $(".full_nav .nav_items").css("height","0")
        mainClicked = false;
    }
    
}

function goToPage(link){
    if(link)
    window.open(link)
}

function previewFile(file){
    $(".bdrop").html("")
    $(".bdrop").css("display","flex")
    $(".bdrop").append(
        "<div>"+
            "<object data='"+file+"' type='application/pdf'>"+
            "<p>Your web browser doesn't have a PDF plugin. Instead you can <a download href=\""+file+"\">click here to download the PDF file.</a></p>"+
            "</object>"+
        "</div>"+
        "<span onclick='closePreview(e)'>Close</span>"
    )
}

function closePreview(e){
    if (e.target !== this){
        $(".bdrop").fadeOut()
    }
}


function cycleImages(){
    var $active = $('#hero_imgs .active');
    var $next = ($active.next().length > 0) ? $active.next() : $('#hero_imgs img:first');
    $next.css('z-index',2);//move the next image up the pile
    $active.fadeOut(1500,function(){//fade out the top image
    $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
        $next.css('z-index',3).addClass('active');//make the next image the top one
    });
}

function checkClasses(){
    var total = $('.latest-work-carousel .owl-stage .owl-item.active').length;

    $('.latest-work-carousel .owl-stage .owl-item').removeClass('firstActiveItem lastActiveItem');

    $('.latest-work-carousel .owl-stage .owl-item.active').each(function(index){
        if (index === 0) {
            // this is the first one
            $(this).addClass('firstActiveItem');
        }
        if (index === total - 1 && total>1) {
            // this is the last one
            $(this).addClass('lastActiveItem');
        }
    });
}

