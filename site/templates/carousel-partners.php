<?php namespace ProcessWire; 
echo '<div class="pc_wrapper">';
echo "<h1>Partners</h1>";
echo '<div class="owl-carousel owl-theme" id="partner_carousel">';
    $partners = $pages->find("template=partners-logo");
    foreach($partners as $p){
        foreach($p->partners_logo as $pl){
            echo '<div class="item">';
            echo '<span class="vlinel"></span>';
            echo '<img onclick="goToPage(\''.$pl->link.'\')" src="'.$pl->logo->url.'"/>';
            echo "<h2>".preg_replace('/\s+/', '&nbsp;', $pl->title)."</h2>";
            echo '<span class="vliner"></span>';
            echo "</div>";
        }
    }
echo "</div>";
echo "</div>";
?>

<!-- onclick="goToPage(\''.$pl->link.'\')"> -->