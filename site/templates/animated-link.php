<?php 
    namespace ProcessWire;
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page->title ?></title>
    <meta name="description" content="Created to distill the lessons learnt and build upon the strengths of the UNITAR Hiroshima Afghan Fellowship. It aspires to channel the bonds, knowledge and energy of this worldwide community towards innovative and sustainable initiatives in Afghanistan.">
    <meta name="author" content="The Afghan Fellowship Legacy Projects (AFLP)">
    <meta name="keywords" content="UNITAR, AFLP, Afghan Fellowship Legacy Projects">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,700|Lexend+Deca&display=swap"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/main.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.theme.default.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates; ?>img/favicon.ico">
    <meta name="theme-color" content="#26292d">
</head>
<body>
    <?php 
        include "nav.php";
        if (in_array($page->title, $hasCarousel)){
            $noCarousel = '';
        }else{
            $noCarousel = ' noCarousel';
        }
    ?>
    <!-- <div class="spacer dark"></div> -->
    <section class="j_width dark j_header">
        <h1 class="main_title" data-aos="fade-down"  data-aos-delay="200"><?php echo $page->title ?></h1>
    </section>

    <?php 
        if (in_array($page->title, $hasCarousel)){
            $n = str_replace('(', '', $page->title);
            $nn = str_replace(')', '', $n);
            $name = 'carousel-'.strtolower(str_replace(' ', '-', $nn));
            $ccenter = '';
            $crect = '';
            include "carousel.php";
        }
    ?>

    <section class="j_width" data-aos="fade-up" data-aos-delay="550">
        <?php
            foreach($page->animated_link as $al){
                echo "<h1 class='main_title centered'>".$al->title."</h1>";
                echo "<div class='animated_link_div'>";
                foreach($al->animated_link_content as $alc){
                    echo "<div class='ald'>";
                    echo "<div class='ald_title'>";
                    echo $alc->title;
                    echo "</div>";
                    echo "<div class='ald_content' >";
                    if($alc->the_file){
                        echo "<a href='".$alc->the_file->httpUrl()."'>".$alc->the_file."</a>";
                    }else{
                        echo "<a href='".$alc->the_url."' target='_blank'>".$alc->the_url."</a>";
                    }
                    echo "</div>";
                    
                    echo "<div class='ald_btn_wrapper'>";
                    if($alc->the_file){
                    echo "<div class='ald_preview' title='Preview file'><a onclick='previewFile(\"".$alc->the_file->httpUrl()."\")'><i class='fas fa-eye'></i><span>Preview</span></a></div>";
                    echo "<div class='ald_download' title='Download file'><a href='".$alc->the_file->httpUrl()."'><i class='fas fa-download'></i><span>Download</
                    span></a></div>";
                    }else{
                        echo "<div class='ald_visit' title='Visit URL'><a href='".$alc->the_url."' target='_blank'><i class='fas fa-globe'></i><span>Visit</
                    span></a></div>";
                    }
                    echo "</div>";
                    echo "</div>";
            }
                echo "</div>";
            }
        ?>
    </section>

    <section class="j_width" >
        <div class="bot_nav">
            <?php

                if($page->prev->url){
                    echo "<a href='".$page->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->prev->title."</a>";
                }else{
                    echo "<a href='".$page->parent()->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->parent()->prev->title."</a>";
                }

                if($page->next->url){
                    if(count($page->next->children()) == 0){
                        echo "<a href='".$page->next->url."'>".$page->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }else{
                        echo "<a href='".$page->next->children()->first()->url."'>".$page->next->children()->first()->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }
                }
            ?>
        </div>
    </section>

    <div class="bdrop" onclick="closePreview(event)"></div>
    

    <?php include "footer.php" ?>

 
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.5.207/pdf.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/owl.carousel.min.js"></script>
    
</body>
</html>