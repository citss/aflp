<?php namespace ProcessWire; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page->title ?></title>
    <meta name="description" content="Created to distill the lessons learnt and build upon the strengths of the UNITAR Hiroshima Afghan Fellowship. It aspires to channel the bonds, knowledge and energy of this worldwide community towards innovative and sustainable initiatives in Afghanistan.">
    <meta name="author" content="The Afghan Fellowship Legacy Projects (AFLP)">
    <meta name="keywords" content="UNITAR, AFLP, Afghan Fellowship Legacy Projects">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,700|Lexend+Deca&display=swap"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/main.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates; ?>img/favicon.ico">
    <meta name="theme-color" content="#26292d">
</head>
<body>
<?php include "nav.php" ?>
    <section class="j_width dark j_header">
        <h1 class="main_title" data-aos="fade-down"  data-aos-delay="200"><?php echo $page->title ?></h1>
    </section>

    <section class="j_width">
        <?php 
            $rp = $pages->find("template=resources-page");
            foreach($rp as $p){
                foreach($p->resources_row as $r){
                    echo "<h1 class='r_title'>".$r->resources_title."</h1>";
                    echo ($r->resources_subtitle ? "<h2 class='r_subtitle'>".$r->resources_subtitle."</h2>" : "");
                    echo ($r->resources_date ? "<h3 class='r_date'>".date("d F Y",strtotime($r->resources_date))."</h3>" : "");
                    echo "<div class='resources_carousel_wrapper'>";
                    echo (count($r->resources_yt_slider) > 1 ? "<span class='rc_left'><i class='fas fa-chevron-left'></i></span>" : "");
                    echo (count($r->resources_yt_slider) > 1 ? "<span class='rc_right'><i class='fas fa-chevron-right'></i></span>" : "");
                    echo "<div class='resources_carousel'>";
                    foreach($r->resources_yt_slider as $yt){
                        echo "<div class='item'>";
                        echo "<div class='video_wrapper'>";
                        echo "<div class='v_curtain'></div>";
                        echo "<iframe src='".$yt->resources_yt_link."' frameBorder='0'>";
                        echo "</iframe>";
                        echo "</div>";
                        echo "</div>";
                    }
                    echo "</div>";

                    if(count($r->resources_file_list) > 0){
                        echo "<div class='resources_file_list'>";
                        foreach($r->resources_file_list as $file){
                            echo "<a href='".$file->resources_file_link."' target='_blank'><i class='fas fa-link'></i>&nbsp;".$file->resources_file_title."</a>";
                        }
                        echo "</div>";
                    }

                    echo "<div class='resources_buttons'>";
                        echo ($r->resources_readmore ? "<a href='".$r->resources_readmore."' target='_blank'>Read more</a>" : "");
                        echo ($r->resources_fullvideo ? "<a href='".$r->resources_fullvideo."' target='_blank'>Full video</a>" : "");
                    echo "</div>";
                    echo "</div>";

                }
            }
        ?>
    </section>

    <section class="j_width" >
        <div class="bot_nav">
            <?php

                if($page->prev->url){
                    echo "<a href='".$page->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->prev->title."</a>";
                }else{
                    echo "<a href='".$page->parent()->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->parent()->prev->title."</a>";
                }

                if($page->next->url){
                    if(count($page->next->children()) == 0){
                        echo "<a href='".$page->next->url."'>".$page->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }else{
                        echo "<a href='".$page->next->children()->first()->url."'>".$page->next->children()->first()->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }
                }else{
                    echo "<a href='".$page->parent()->next->url."'>".$page->parent()->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                }
            ?>
        </div>
    </section>
    

    <?php include "footer.php" ?>

 
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> 
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.resources_carousel').slick({
                slidesToShow: 1,
                adaptiveHeight: true,
                infinite: true,
                autoplay: true,
                arrows: false,
                autoplaySpeed: 3000,
                speed: 2000,
                pauseOnHover:false
            })

            $('.resources_carousel_wrapper .rc_left').click(function(){
                $(this).next().next().slick("slickPrev");
            });

            $('.resources_carousel_wrapper .rc_right').click(function(){
                $(this).next().slick("slickNext");
            });

        })

        $(document).on("click", ".v_curtain", function () {
            let v_curtain =  $(this);
            v_curtain.hide()

            let iframe = v_curtain.next();
            // let url =  iframe.prop("src");
            // url += "?autoplay=1";        
            // iframe.prop("src",url)

            var slick_carousel = v_curtain.parent().parent().parent().parent().parent()
            slick_carousel.slick('slickPause');

            $('.resources_carousel_wrapper .rc_left, .resources_carousel_wrapper .rc_right').click(function(){
                v_curtain.show();
                slick_carousel.slick("slickPlay");
                let y_url =  iframe.prop("src");
                y_url = y_url.replace("?autoplay=1", "");
                iframe.prop("src",y_url)
            });

        })

    </script>
</body>
</html>