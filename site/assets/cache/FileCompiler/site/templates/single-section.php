<?php namespace ProcessWire; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page->title ?></title>
    <meta name="description" content="Created to distill the lessons learnt and build upon the strengths of the UNITAR Hiroshima Afghan Fellowship. It aspires to channel the bonds, knowledge and energy of this worldwide community towards innovative and sustainable initiatives in Afghanistan.">
    <meta name="author" content="The Afghan Fellowship Legacy Projects (AFLP)">
    <meta name="keywords" content="UNITAR, AFLP, Afghan Fellowship Legacy Projects">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,700|Lexend+Deca&display=swap"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/main.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates; ?>img/favicon.ico">
    <meta name="theme-color" content="#26292d">
</head>
<body>
    <?php 
        if($page->hasChildren()) {
            // Yes, we should show navigation to children
            echo $page->children()->first->url;
            header("Location: ".$page->children()->first->url);
        }

        include "nav.php";
        if (in_array($page->title, $hasCarousel)){
            $noCarousel = '';
        }else{
            $noCarousel = ' noCarousel';
        }
    ?>
    <!-- <div class="spacer dark"></div> -->
    <section class="j_width dark j_header <?php echo $noCarousel ?>">
        <h1 class="header_title" data-aos="fade-down"  data-aos-delay="200"><?php echo $page->title ?></h1>
        <div class="hq">
            <div class="h_quote"><?php echo $page->header_quote ?></div>
            <div class="h_quote_author"><?php echo $page->header_quote_author ?></div>
        </div>
    </section>

    <?php
        if(strpos($page->title, 'Botanical Gardens Network') !== false){
            include "carousel-partners.php";
        }
    ?>

    <?php 
        if (in_array($page->title, $hasCarousel)){
            $n = str_replace('(', '', $page->title);
            $nn = str_replace(')', '', $n);
            $name = 'carousel-'.strtolower(str_replace(' ', '-', $nn));
            $ccenter = '';
            $crect = '';
            include "carousel.php";
        }

        if(strpos($_SERVER['REQUEST_URI'], '/partners/in-afghan/')){
            echo '<div class="pc_wrapper">';
            echo "<h1>Partners in Afghanistan</h1>";
            echo '<div class="owl-carousel owl-theme" id="partner_carousel">';

            $partners = $pages->find("template=partners-logo");
            foreach($partners as $p){
                foreach($p->partners_logo as $pl){
                    if($pl->partner_type == 'Afghan'){ 
                        echo '<div class="item">';
                        echo '<span class="vlinel"></span>';
                        echo '<img onclick="goToPage(\''.$pl->link.'\')" src="'.$pl->logo->url.'"/>';
                        echo "<h2>".preg_replace('/\s+/', '&nbsp;', $pl->title)."</h2>";
                        echo '<span class="vliner"></span>';
                        echo "</div>";
                    }
                }
            }
            echo "</div>";
            echo "</div>";

            $ccenter = ' mtop';
            $crect = 'crect';
            $name = 'carousel-partners-in-afghanistan';
            include "carousel.php";
        }else if(strpos($_SERVER['REQUEST_URI'], '/partners/international/')){
            echo '<div class="pc_wrapper">';
            echo "<h1>Partners Worldwide</h1>";
            echo '<div class="owl-carousel owl-theme" id="partner_carousel">';

            $partners = $pages->find("template=partners-logo");
            foreach($partners as $p){
                foreach($p->partners_logo as $pl){
                    if($pl->partner_type == 'International'){ 
                        echo '<div class="item">';
                        echo '<span class="vlinel"></span>';
                        echo '<img onclick="goToPage(\''.$pl->link.'\')" src="'.$pl->logo->url.'"/>';
                        echo "<h2>".preg_replace('/\s+/', '&nbsp;', $pl->title)."</h2>";
                        echo '<span class="vliner"></span>';
                        echo "</div>";
                    }
                }
            }
            echo "</div>";
            echo "</div>";
        }
    ?>
    

    <section class="j_width" data-aos="fade-up"  data-aos-delay="700">
        <?php echo $page->body ?> 
    </section>

    <section class="j_width" >
        <div class="bot_nav">
            <?php

                if($page->prev->url){
                    echo "<a href='".$page->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->prev->title."</a>";
                }else{
                    echo "<a href='".$page->parent()->prev->url."'><i class='fas fa-chevron-left'></i>&nbsp;".$page->parent()->prev->title."</a>";
                }

                if($page->next->url){
                    if(count($page->next->children()) == 0){
                        echo "<a href='".$page->next->url."'>".$page->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }else{
                        echo "<a href='".$page->next->children()->first()->url."'>".$page->next->children()->first()->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }
                }else{
                    if($page->parent()->next->title != 'Login'){
                        echo "<a href='".$page->parent()->next->url."'>".$page->parent()->next->title."&nbsp;<i class='fas fa-chevron-right'></i></a>";
                    }
                }
            ?>
        </div>
    </section>
    

    <?php include "footer.php" ?>

 
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> 
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/owl.carousel.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
</body>
</html>