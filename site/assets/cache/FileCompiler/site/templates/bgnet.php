<?php namespace ProcessWire; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AFLP - BGNet</title>
    <meta name="description" content="Created to distill the lessons learnt and build upon the strengths of the UNITAR Hiroshima Afghan Fellowship. It aspires to channel the bonds, knowledge and energy of this worldwide community towards innovative and sustainable initiatives in Afghanistan.">
    <meta name="author" content="The Afghan Fellowship Legacy Projects (AFLP)">
    <meta name="keywords" content="UNITAR, AFLP, Afghan Fellowship Legacy Projects">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,700|Lexend+Deca&display=swap"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/main.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $config->urls->templates; ?>styles/owl.theme.default.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates; ?>img/favicon.ico">
    <meta name="theme-color" content="#26292d">
</head>
<body>
    <?php include "nav.php" ?>
    <!-- <div class="spacer dark"></div> -->
    <section class="j_width dark j_header">
        <h1 class="main_title" class="header_title" data-aos="fade-down"  data-aos-delay="200"><?php echo $page->title ?></h1>
    </section>

    <section class="j_width" data-aos="fade-up"  data-aos-delay="700">
        
        <h1 id="introduction" class="bgnet_title"  data-aos="fade-up" data-aos-delay="550" data-aos-offset="0"><?php echo $page->children->first()->title ?> </h1>
        <?php echo $page->children->first()->body ?> 
    </section>
    

    <section class="j_width">
        <?php 
            $partners = $pages->find("template=partners-logo");
        ?>
        <h1 class="bgnet_title" id="partners" data-aos="fade-up" data-aos-delay="550" data-aos-offset="0"><?php 
            foreach($partners as $p){
                echo $p->title; 
            }
        ?></h1>
        <div class="partner_row" data-aos="fade-up" data-aos-delay="550" data-aos-offset="0">
            <?php 
                foreach($partners as $p){
                    foreach($p->partners_logo as $pl){
                        echo '<div class="partner_col" onclick="goToPage(\''.$pl->link.'\')">';
                        echo "<div class='line_top'></div>";
                        echo "<img src='".$pl->logo->url."'/>";
                        echo "<h2>".$pl->title."</h2>";
                        echo "<div class='line_bot'></div>";
                        echo "</div>";
                    }
                }
            ?>
            <div class='partner_col'></div>
            <div class='partner_col'></div>
        </div>
    </section>
    <section class="j_width">
        <h1 class="bgnet_title" id="launch_activities" data-aos="fade-up" data-aos-delay="550" data-aos-offset="0">Launch</h1>
        <div class="seminar_row" data-aos="fade-up" data-aos-delay="550" data-aos-offset="0">
        <?php 
            $seminars = $pages->find("template=seminars");
            foreach($seminars as $s){
                echo "<div class='seminar_col'>";
                echo "<img src='".$s->main_photo->url."'/>";
                echo "<h2>".$s->title."</h2>";
                echo "<p>".$s->summary."</p>";
                echo "<a href='".$s->url."'>Read more</a>";
                echo "</div>";
                
            }
        ?>
        <div class='seminar_col'></div>
        </div>
    </section>
    
    

    <?php include "footer.php" ?>

 
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> 
    <script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?php echo $config->urls->templates; ?>scripts/owl.carousel.min.js"></script>
</body>
</html>